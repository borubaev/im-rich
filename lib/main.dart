import 'package:flutter/material.dart';

void main() {
  runApp(const MyApp());
}

class MyApp extends StatelessWidget {
  const MyApp({super.key});

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'lesson3',
      theme: ThemeData(
        primarySwatch: Colors.blue,
      ),
      home: const Home(title: 'Тапшырма 3'),
      debugShowCheckedModeBanner: false,
    );
  }
}

class Home extends StatefulWidget {
  const Home({super.key, required this.title});

  final String title;

  @override
  State<Home> createState() => Main();
}

class Main extends State<Home> {
  // int _counter = 0;

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.yellow,
      appBar: AppBar(
        backgroundColor: Colors.yellow,
        elevation: 1,
        title: Center(
            child: Text(
          widget.title,
          style: const TextStyle(color: Colors.black),
        )),
        shadowColor: Colors.grey,
      ),
      body: Column(
        mainAxisAlignment: MainAxisAlignment.center,
        children: [
          Padding(
            padding: const EdgeInsets.all(12),
            child: Center(
              child: Container(
                width: double.infinity,
                decoration:
                    BoxDecoration(borderRadius: BorderRadius.circular(7)),
                child: const Center(
                    child: Text(
                  "I'm Shaiyrbek",
                  style: TextStyle(
                    fontSize: 30.5,
                    fontFamily: 'Sofia',
                    // fontStyle: FontStyle.italic,
                    fontWeight: FontWeight.normal,
                  ),
                )),
              ),
            ),
          ),
          Center(
            child: Column(
              children: [
                Image.network(
                  'https://s.yimg.com/ny/api/res/1.2/1hDbPB2hmVn_Zhy2qMkSaQ--/YXBwaWQ9aGlnaGxhbmRlcjt3PTY0MDtoPTMzNQ--/https://media.zenfs.com/en/financebuzz_470/384c066823bdce4dada57882c3b2709b',
                  width: 350,
                ),
              ],
            ),
          )
        ],
      ),
    );
  }
}
